<?php

/**
 * Interface
 * @author WYZEN
 *
 */

declare(strict_types=1);

namespace Wyzen\Doctrine\SimpleQueryBuilder;

use Doctrine\DBAL\Connection;
use Wyzen\Doctrine\SimpleQueryBuilder\SimpleQueryBuilderOperator;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Interface
 */
interface SimpleQueryBuilderInterface
{
    /**
     * constructor
     *
     * @param Connection $conn
     * @param string|null $className
     */
    public function __construct(Connection $conn, ?string $className = null);

    /**
     * Ajoute une table
     *
     * @param [type] $tables
     * @return SimpleQueryBuilderAbstract
     */
    public function setTable(string $table): SimpleQueryBuilderAbstract;

    /**
     * Récupère le from
     *
     * @return String
     */
    public function getTable(): string;


    /**
     * Ajoute plusieurs fields au select
     *
     * @param String $field
     * @return SimpleQueryBuilderAbstract
     */
    public function addFields(array $field): SimpleQueryBuilderAbstract;

    /**
     * Ajoute un field
     *
     * @param String $colName
     * @return SimpleQueryBuilderAbstract
     */
    public function addField(string $colName): SimpleQueryBuilderAbstract;

    /**
     * Récupère la liste des fields du select
     *
     * @return array
     */
    public function getFields(): array;

    /**
     * Ajoute un where
     *
     * @param String $field le champ sur lequel le where s'effectue
     * @param [type] $value la valeur à filtrer
     * @param String $operator= (DtoOperator::EQ), IN (DtoOperator::IN), Between (DtoOperator::BETWEEN), etc
     * @param bool $noCase (true pour ne vas avoir la verif de la casse pour recherche par ex)
     * @return SimpleQueryBuilderAbstract
     */
    public function addFilter(string $field, $value, string $dtoOperator = SimpleQueryBuilderOperator::EQ, ?bool $noCase = false): SimpleQueryBuilderAbstract;

    /**
     * Ajoute un where ( sans ajouter le filed dans le nom de var)
     *
     * @param String $field le champ sur lequel le where s'effectue
     * @param [type] $value la valeur à filtrer
     * @param String $operator= (DtoOperator::EQ), IN (DtoOperator::IN), Between (DtoOperator::BETWEEN), etc
     * @param bool $noCase (true pour ne vas avoir la verif de la casse pour recherche par ex)
     * @return SimpleQueryBuilderAbstract
     */
    public function addCustomFilter(string $field, $value, string $dtoOperator = SimpleQueryBuilderOperator::EQ, ?bool $noCase = false): SimpleQueryBuilderAbstract;


    /**
     * Récupération des where
     *
     * @return array
     */
    public function getFilters(): array;


    /**
     * Ajoute un order by
     *
     * @param String $order
     * @param string $ascDesc
     * @return SimpleQueryBuilderAbstract
     */
    public function addOrder(string $order, ?string $ascDesc = 'asc'): SimpleQueryBuilderAbstract;

    /**
     * Ajoute un groupby
     *
     * @param [type] $field
     * @return SimpleQueryBuilderAbstract
     */
    public function addGroup(string $field): SimpleQueryBuilderAbstract;

    /**
     * Création de la requête
     *
     * @return ForwardCompatibility\DriverStatement|int
     */
    public function execute();

    /**
     * Return count row without pagination
     *
     * @param string|null $column_name '*' if column_name is null
     *
     * @return integer
     */
    public function executeRowCount(?string $column_name = null): int;

    /**
     * Execute la requête et retourne toutes les lignes trouvées
     *
     * @param string|null $column_name '*' if column_name is null
     *
     * @return array
     */
    public function fetchAll(?string $column_name = null): array;

    /**
     * Transforme un classname en nom de vue
     *
     * @return String
     */
    public function transformClassName(string $className): string;

    /**
     * Réinitialise tous les params
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearAll(): SimpleQueryBuilderAbstract;

    /**
     * Réinitialise le select
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearFields(): SimpleQueryBuilderAbstract;

    /**
     * Réinitialise le where
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearFilters(): SimpleQueryBuilderAbstract;

    /**
     * Réinitialise le from
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearFrom(): SimpleQueryBuilderAbstract;


    /**
     * Réinitialise le order by
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearOrder(): SimpleQueryBuilderAbstract;

    /**
     * Réinitialise le group by
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearGroup(): SimpleQueryBuilderAbstract;

    /**
     * Recupère le QueryBuilder
     *
     * @return QueryBuilder
     */
    public function getQb(): QueryBuilder;

    /**
     * Set la limit
     *
     * @param integer $limit
     * @return SimpleQueryBuilderAbstract
     */
    public function setLimit(int $limit): SimpleQueryBuilderAbstract;

    /**
     * Créé le SQL
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function buildQuery(): SimpleQueryBuilderAbstract;

    /**
     * Build les clauses avec du only andWhere ou du only orWhere
     *
     * @param String $params
     * @return SimpleQueryBuilderAbstract
     */
    public function buildCondition(string $params): SimpleQueryBuilderAbstract;

    /**
     * Modifie la condition pour build les clauses (And ou Or)
     *
     * @param String $condition
     * @return SimpleQueryBuilderAbstract
     */
    public function setCondition(string $condition): SimpleQueryBuilderAbstract;

    /**
     * Récupère la condition pour build les clauses (And ou Or)
     *
     * @return String
     */
    public function getCondition(): string;

    /**
     * Test if query is Distinct
     *
     * @return boolean
     */
    public function isDistinct(): bool;

    /**
     * Set query as distinct
     *
     * @param boolean $distinct
     *
     * @return self
     */
    public function setDistinct(bool $distinct = true): SimpleQueryBuilderAbstract;
}
