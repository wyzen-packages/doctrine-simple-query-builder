<?php

/**
 * Exception
 * @author WYZEN
 *
 */

declare(strict_types=1);

namespace Wyzen\Doctrine\SimpleQueryBuilder;

use Exception;

class SimpleQueryBuilderException extends Exception
{
    public $code    = 500;
    public $message = 'ERROR_QUERY';
}
