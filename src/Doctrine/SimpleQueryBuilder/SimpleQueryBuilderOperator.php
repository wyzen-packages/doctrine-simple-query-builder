<?php

/**
 * Operators
 * @author WYZEN
 *
 */

declare(strict_types=1);

namespace Wyzen\Doctrine\SimpleQueryBuilder;

final class SimpleQueryBuilderOperator
{
    /**
     * OPERATOR
     * @var string
     */
    public const EQ         = '##OP#EQ##';
    public const NE         = '##OP#NE##';
    public const LT         = '##OP#LT##';
    public const LE         = '##OP#LE##';
    public const GT         = '##OP#GT##';
    public const GE         = '##OP#GE##';
    public const IN         = '##OP#IN##';
    public const NOTIN      = '##OP#NOTIN##';
    public const BETWEEN    = '##OP#BETWEEN##';
    public const NOTBETWEEN = '##OP#NOTBETWEEN##';
    public const ISNULL     = '##OP#ISNULL##';
    public const ISNOTNULL  = '##OP#ISNOTNULL##';
    public const LIKE       = '##OP#LIKE##';
    public const NOTLIKE    = '##OP#NOTLIKE##';
    public const CUSTOM    = '##OP#CUSTOM##';

    /**
     * UNION
     */
    // TODO UNION
    // const UNION = '##Q#UNION##';
    // const UNIONALL = '##Q#UNIONALL##';

    /**
     * INSERT, IGNORE, OR UPDATE
     */
    // TODO INSERT, IGNORE, OR UPDATE
    // const IGNORE = '##INSERT#IGNORE##';
    // const DUPLICATEKEY = '##INSERT#OR#UPDATE##';

    /**
     *
     * WHERE
     */
    public const NONE  = "";
    public const WHERE = 'WHERE';
    public const AND   = 'AND';
    public const OR    = 'OR';

    /**
     * Return the translate operator
     *
     * @param awcQBop ::<const> $operator
     *
     * @return bool|string
     */
    public static function isOp($operator)
    {
        if (!is_string($operator)) {
            return false;
        }

        switch ((string)$operator) {
            case self::EQ:
            case self::LT:
            case self::LE:
            case self::GT:
            case self::GE:
            case self::NE:
            case self::IN:
            case self::NOTIN:
            case self::ISNULL:
            case self::ISNOTNULL:
            case self::LIKE:
            case self::NOTLIKE:
            case self::BETWEEN:
            case self::NOTBETWEEN:
                return true;
                break;
        }
        return false;
    }

    /**
     * Return the correct $str
     *
     * @param unknown_type $str
     *
     * @return bool|string
     */
    public static function str($str)
    {
        return (is_string($str)) ? "'" . str_replace("'", "''", $str) . "'" : false;
    }
}
