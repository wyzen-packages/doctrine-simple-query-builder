<?php

/**
 * Abstract
 * @author WYZEN
 *
 */

declare(strict_types=1);

namespace Wyzen\Doctrine\SimpleQueryBuilder;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Query\Expr;

/**
 * Abstract class
 */
abstract class SimpleQueryBuilderAbstract implements SimpleQueryBuilderInterface
{
    /** @var Connection */
    protected $conn = null;

    /** @var QueryBuilder */
    protected $qb;

    protected $isDistinct = false;
    protected $fields     = [];
    protected $table      = null;
    protected $where      = [];
    protected $orderby    = [];
    protected $groupby    = [];
    protected $condition  = SimpleQueryBuilderOperator::AND;


    /** @var Pagination */
    protected $pagination;

    public function __construct(Connection $conn, ?string $className = null)
    {
        $this->pagination = new Pagination();

        $this->conn = $conn;

        /** @var QueryBuilder */
        $this->qb = $conn->createQueryBuilder();

        if (!\is_null($className)) {
            $vue = $this->transformClassName($className);
            $this->setTable($vue);
        }

        $this->condition = SimpleQueryBuilderOperator::AND;
        $this->fields[]  = '*';
    }

    /**
     * Return Connection
     *
     * @return \Doctrine\DBAL\Connection
     */
    public function getConn(): Connection
    {
        return $this->conn;
    }

    /**
     * Ajoute un from
     *
     * @param [type] $table
     * @return SimpleQueryBuilderAbstract
     */
    public function setTable(string $table): SimpleQueryBuilderAbstract
    {
        $vue         = $this->transformClassName($table);
        $this->table = $vue;

        return $this;
    }

    /**
     * Récupère la table/vue
     *
     * @return String
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * Récupère le Query Builder
     *
     * @return QueryBuilder
     */
    public function getQb(): QueryBuilder
    {
        return $this->qb;
    }

    /**
     * Ajoute une clause select
     *
     * @param String $colName
     * @return SimpleQueryBuilderAbstract
     */
    public function addField(string $colName): SimpleQueryBuilderAbstract
    {
        if ($this->fields[0] === "*" || $this->fields[0] === 'count(*)') {
            $this->fields[0] = $colName;
        } else {
            $this->fields[] = $colName;
        }
        return $this;
    }

    /**
     * Ajoute plusieurs clauses select
     *
     * @param array $fields
     * @return SimpleQueryBuilderAbstract
     */
    public function addFields(array $fields): SimpleQueryBuilderAbstract
    {
        foreach ($fields as $field) {
            if ($this->fields[0] === "*" || $this->fields[0] === 'count(*)') {
                $this->fields[0] = $field;
            } else {
                $this->fields[] = $field;
            }
        }
        return $this;
    }

    /**
     * Récupère la liste des select
     *
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function setDistinct(bool $distinct = true): self
    {
        $this->isDistinct = $distinct;
        return $this;
    }

    public function isDistinct(): bool
    {
        return $this->isDistinct;
    }

    protected function buildFilter(string $field, $value, string $operator = SimpleQueryBuilderOperator::EQ, ?bool $noCase = false): array
    {
        $valueIsOperator = false;
        // Cas du Is null / Is not null
        if (SimpleQueryBuilderOperator::isOp($value)) {
            $operator        = $value;
            $value           = null;
            $valueIsOperator = true;
        }

        /**
         * Finalement $value on passe un tableau donc conversion en IN ou NOTIN
         */
        if (\is_array($value) && ($operator === SimpleQueryBuilderOperator::EQ || $operator === SimpleQueryBuilderOperator::NE)) {
            if ($operator === SimpleQueryBuilderOperator::EQ) {
                $operator = SimpleQueryBuilderOperator::IN;
            } elseif ($operator === SimpleQueryBuilderOperator::NE) {
                $operator = SimpleQueryBuilderOperator::NOTIN;
            } else {
                throw new SimpleQueryBuilderException();
            }
        } elseif ($valueIsOperator === false && \is_null($value)) {
            $operator = SimpleQueryBuilderOperator::ISNULL;
        }

        switch ($operator) {
            default:
                $operator = SimpleQueryBuilderOperator::EQ;
            case SimpleQueryBuilderOperator::EQ:
            case SimpleQueryBuilderOperator::GT:
            case SimpleQueryBuilderOperator::LT:
            case SimpleQueryBuilderOperator::LE:
            case SimpleQueryBuilderOperator::GE:
            case SimpleQueryBuilderOperator::NE:
            case SimpleQueryBuilderOperator::ISNULL:
            case SimpleQueryBuilderOperator::ISNOTNULL:
            case SimpleQueryBuilderOperator::LIKE:
            case SimpleQueryBuilderOperator::NOTLIKE:
                if (\is_array($value)) {
                    throw new SimpleQueryBuilderException();
                }
                break;
            case SimpleQueryBuilderOperator::IN:
            case SimpleQueryBuilderOperator::NOTIN:
                if (!\is_array($value)) {
                    throw new SimpleQueryBuilderException();
                }
                break;
            case SimpleQueryBuilderOperator::BETWEEN:
            case SimpleQueryBuilderOperator::NOTBETWEEN:
                if (!\is_array($value) || (\is_array($value) && \count($value) !== 2)) {
                    throw new SimpleQueryBuilderException();
                }
                break;
        }

        return  [
            'field' => $field,
            'value' => \is_bool($value) ? \intval($value) : $value,
            'operator' => $operator,
            'noCase' => $noCase,
        ];
    }

    /**
     * Ajoute une clause where
     *
     * @param String $field
     * @param [type] $value
     * @param String $operator
     * @return SimpleQueryBuilderAbstract
     */
    public function addCustomFilter(string $field, $value = null, string $operator = SimpleQueryBuilderOperator::EQ, ?bool $noCase = false): SimpleQueryBuilderAbstract
    {
        $ret = $this->buildFilter('(' . $field . ')', \is_null($value) ? SimpleQueryBuilderOperator::CUSTOM : $value, $operator, $noCase);
        $ret['customFilter'] = true;

        $this->where[] = $ret;

        return $this;
    }

    /**
     * Ajoute une clause where
     *
     * @param String $field
     * @param [type] $value
     * @param String $operator
     * @return SimpleQueryBuilderAbstract
     */
    public function addFilter(string $field, $value, string $operator = SimpleQueryBuilderOperator::EQ, ?bool $noCase = false): SimpleQueryBuilderAbstract
    {
        $ret           = $this->buildFilter($field, $value, $operator, $noCase);
        $this->where[] = $ret;

        return $this;
    }

    /**
     * Récupère la liste des clauses where
     *
     * @return array
     */
    public function getFilters(): array
    {
        return $this->where;
    }

    public function getWhere(): array
    {
        return $this->where;
    }
    public function getOrderby(): array
    {
        return $this->orderby;
    }
    public function getGroupby(): array
    {
        return $this->groupby;
    }

    /**
     * Ajoute un order by
     *
     * @param String $field
     * @param String|null $ascDesc
     * @return SimpleQueryBuilderAbstract
     */
    public function addOrder(string $field, ?string $ascDesc = 'asc'): SimpleQueryBuilderAbstract
    {
        $this->orderby[] = [
            'field' => $field,
            'asc' => $ascDesc
        ];

        return $this;
    }

    /**
     * Ajoute un group by
     *
     * @param [type] $field
     * @return SimpleQueryBuilderAbstract
     */
    public function addGroup(string $field): SimpleQueryBuilderAbstract
    {
        $this->groupby[] = $field;

        return $this;
    }

    /**
     * Ajoute un group by
     *
     * @param [type] $field
     * @return SimpleQueryBuilderAbstract
     */
    public function addGroups(array $fields = []): SimpleQueryBuilderAbstract
    {
        foreach ($fields as $field) {
            $this->addGroup($field);
        }
        return $this;
    }

    /**
     * return the slug of string
     *
     * @param string $text
     * @param string $divider
     *
     * @return string
     */
    public static function slugify(string $text, string $divider = '_'): string
    {
        if (empty($text)) {
            return 'n' . $divider . 'a';
        }

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', $divider, $text)));
        // Supprime le divider de debut et de fin
        $text = preg_replace("~^$divider~", '', $text);
        $text = preg_replace("~$divider\$~", '', $text);

        return $text;

        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n' . $divider . 'a';
        }

        return $text;
    }


    /**
     * Création de la requête SQL
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function buildQuery(): SimpleQueryBuilderAbstract
    {
        /** @var Expr */
        $expr = new Expr();
        self::clearQb($this->qb);

        /**
         * Assemblage des tous les tables
         * @var null|array $field
         */
        foreach ($this->fields as $field) {
            if (isset($field['alias'])) {
                $this->qb->addSelect(implode(' AS ', $field));
            } else {
                $this->qb->addSelect($field);
            }
        }

        if ($this->table !== "") {
            $this->qb->from($this->table);
        }

        if (count($this->where)) {
            foreach ($this->where as $key => $clause) {

                /**
                 * Création d'un index de clé pour les valeurs
                 */
                if (isset($clause['customFilter'])) {
                    $bindParamParameter = 'customFilter_' . $key;
                    // Si la value est de type CUSTOM, on modifie l'operateur
                    if ($clause['value'] === SimpleQueryBuilderOperator::CUSTOM) {
                        $clause['operator'] = SimpleQueryBuilderOperator::CUSTOM;
                    }
                } else {
                    $bindParamParameter = self::slugify($clause['field']) . '_' . $key;
                }
                $bindParam          = ':' . $bindParamParameter;

                switch ($clause['operator']) {
                    case SimpleQueryBuilderOperator::CUSTOM:
                        /**
                         * Si l'operateur est de type CUSTOM (sans value, ni operateur)
                         * on place directement l'expression
                         */
                        $this->buildCondition($clause['field']);
                        break;

                    case SimpleQueryBuilderOperator::EQ:
                        $this->buildCondition($expr->eq($clause['noCase'] == true ? "LOWER(" . $clause['field'] . ")" : $clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['noCase'] == true ? strtolower($clause['value']) : $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::GT:
                        $this->buildCondition($expr->gt($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::LT:
                        $this->buildCondition($expr->lt($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::LE:
                        $this->buildCondition($expr->lte($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::GE:
                        $this->buildCondition($expr->gte($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::NE:
                        $this->buildCondition($expr->neq($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::IN:
                        $this->buildCondition($expr->in($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value'], Connection::PARAM_STR_ARRAY);
                        break;
                    case SimpleQueryBuilderOperator::NOTIN:
                        $this->buildCondition($expr->notIn($clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['value'], Connection::PARAM_STR_ARRAY);
                        break;
                    case SimpleQueryBuilderOperator::BETWEEN:
                        $this->buildCondition($expr->between($clause['field'], $bindParam . "1", $bindParam . "2"));
                        $this->qb->setParameter($bindParamParameter . "1", $clause['value'][0]);
                        $this->qb->setParameter($bindParamParameter . "2", $clause['value'][1]);
                        break;
                    case SimpleQueryBuilderOperator::NOTBETWEEN:
                        $this->buildCondition($expr->lt($clause['field'], $bindParam . "1")->__toString());
                        $this->buildCondition($expr->gt($clause['field'], $bindParam . "2")->__toString());
                        $this->qb->setParameter($bindParamParameter . "1", $clause['value'][0]);
                        $this->qb->setParameter($bindParamParameter . "2", $clause['value'][1]);
                        break;
                    case SimpleQueryBuilderOperator::ISNULL:
                        $this->buildCondition($expr->isNull($clause['field']));
                        break;
                    case SimpleQueryBuilderOperator::ISNOTNULL:
                        $this->buildCondition($expr->isNotNull($clause['field']));
                        break;
                    case SimpleQueryBuilderOperator::LIKE:
                        $this->buildCondition($expr->like($clause['noCase'] == true ? "LOWER(" . $clause['field'] . ")" : $clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['noCase'] == true ? strtolower($clause['value']) : $clause['value']);
                        break;
                    case SimpleQueryBuilderOperator::NOTLIKE:
                        $this->buildCondition($expr->notlike($clause['noCase'] == true ? "LOWER(" . $clause['field'] . ")" : $clause['field'], $bindParam)->__toString());
                        $this->qb->setParameter($bindParamParameter, $clause['noCase'] == true ? strtolower($clause['value']) : $clause['value']);
                        break;
                }
            }
        }

        if ($this->isDistinct()) {
            $this->qb->distinct();
        }

        if (count($this->orderby)) {
            foreach ($this->orderby as $key => $order) {
                $this->qb->addOrderBy($order['field'], $order['asc']);
            }
        }
        if (count($this->groupby)) {
            $this->qb->addGroupBy(implode(', ', $this->groupby));
        }

        /** @var Pagination */
        if ($this->pagination->isActive()) {
            $page   = $this->pagination->getPage();
            $limit  = $this->pagination->getLimit();
            $offset = ($page - 1) * $limit;
            $this->qb->setFirstResult($offset)->setMaxResults($limit);
        }

        return $this;
    }

    /**
     * Set le nombre max de résultats à retourner
     *
     * @param integer $limit
     * @return SimpleQueryBuilderAbstract
     */
    public function setLimit(int $limit): SimpleQueryBuilderAbstract
    {
        $this->pagination->setLimit($limit);
        return $this;
    }

    /**
     * Set Pagination
     *
     * @param Pagination $pagination
     *
     * @return self
     */
    public function setPagination(Pagination $pagination): self
    {
        $this->pagination = $pagination;
        return $this;
    }

    /**
     * Set la page actuelle
     *
     * @param integer $page
     * @return SimpleQueryBuilderAbstract
     */
    public function setPage(int $page): SimpleQueryBuilderAbstract
    {
        $this->pagination->setPage($page);
        return $this;
    }


    /**
     * Transforme un nom de repo en nommage SQL
     *
     * @param string $className
     * @return String
     */
    public function transformClassName(string $className): string
    {
        // On cherche si le className contient Repository à la fin
        $classNameExploded = explode('\\', $className);
        $lastValue         = end($classNameExploded);

        if (\preg_match('/(.*)DtoRepository$/', $lastValue, $matches)) {
            $lastValue     = trim($matches[1]);
            $to_underscore = preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $lastValue);
            return 'v_dto' . \strtolower($to_underscore);
        }
        if (\preg_match('/(.*)Repository$/', $lastValue, $matches)) {
            $lastValue     = trim($matches[1]);
            $to_underscore = preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $lastValue);
            return \substr(\strtolower($to_underscore), 1);
        }

        return trim($lastValue);
    }

    /**
     * clear QueryBuilder object
     *
     * @param \Doctrine\DBAL\Query\QueryBuilder $qb
     *
     * @return void
     */
    public static function clearQb(QueryBuilder &$qb): void
    {
        $qb
            ->resetQueryParts()
            ->setMaxResults(null)
            ->setFirstResult(0)
            ->setParameters([]);
    }

    /**
     * Clear les différents éléments de la requête, sauf la table
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearAll(): SimpleQueryBuilderAbstract
    {
        $this->pagination->clear();
        $this
            ->setDistinct(false)
            ->clearFields()
            ->clearFrom()
            ->clearFilters()
            ->clearOrder()
            ->clearGroup();

        self::clearQb($this->qb);
        return $this;
    }

    /**
     * Clear le where
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearFilters(): SimpleQueryBuilderAbstract
    {
        $this->where = [];
        return $this;
    }

    /**
     * Clear le group
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearGroup(): SimpleQueryBuilderAbstract
    {
        $this->groupby = [];

        return $this;
    }

    /**
     * Clear le select
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearFields(): SimpleQueryBuilderAbstract
    {
        $this->fields = ["*"];

        return $this;
    }

    /**
     * Clear le from
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearFrom(): SimpleQueryBuilderAbstract
    {
        $this->qb->resetQueryPart('from');

        return $this;
    }

    /**
     * Clear le order
     *
     * @return SimpleQueryBuilderAbstract
     */
    public function clearOrder(): SimpleQueryBuilderAbstract
    {
        $this->orderby = [];

        return $this;
    }

    /**
     * Création de la requête
     *
     * @param string $column_name
     * @return ForwardCompatibility\DriverStatement|int
     */
    public function execute(?string $column_name = null)
    {
        $this->buildQuery();

        /** @var Pagination */
        if ($this->pagination->isActive()) {
            $rowCount = $this->executeRowCount($column_name);
            $this->pagination->setResults($rowCount);
        }
        try {
            $result = $this->qb->executeQuery();
        } catch (\Exception $ex) {
            throw new SimpleQueryBuilderException($ex->getMessage(), 500);
        }

        return $result;
    }

    /**
     * Execute la requête et retourne toutes les lignes trouvées
     * @param string $column_name
     * @return array
     */
    public function fetchAll(?string $column_name = null): array
    {
        return $this->execute($column_name)->fetchAllAssociative();
    }

    /**
     * Création de la requête (ne retourne que le rowCount)
     *
     * @return integer
     */
    public function executeRowCount(?string $column_name = null): int
    {
        $field = ($column_name) ? 'distinct ' . $column_name : '*';
        $this->buildQuery();
        $qb = clone $this->qb;
        $qb
            ->resetQueryParts(['select', 'orderBy'])
            ->select("count($field) as nb")
            ->setMaxResults(null)
            ->setFirstResult(0);

        try {
            $rowCount = $qb->execute()->fetchAssociative();
        } catch (\Exception $ex) {
            throw new SimpleQueryBuilderException($ex->getMessage(), 500);
        }

        return \intval($rowCount['nb']);
    }

    /**
     * Build les clauses avec du only andWhere ou du only orWhere
     *
     * @param [string|Expr\Comparison] $params
     * @return SimpleQueryBuilderAbstract
     */
    public function buildCondition($params): SimpleQueryBuilderAbstract
    {
        if ($this->condition === SimpleQueryBuilderOperator::OR) {
            $this->qb->orWhere($params);
        } else {
            $this->qb->andWhere($params);
        }
        return $this;
    }

    /**
     * Modifie la condition pour build les clauses (And ou Or)
     *
     * @param String $condition
     * @return SimpleQueryBuilderAbstract
     */
    public function setCondition(string $condition): SimpleQueryBuilderAbstract
    {
        $this->condition = $condition;

        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getPage()
    {
        return $this->pagination->getPage();
    }
    /**
     * Récupère la condition pour build les clauses (And ou Or)
     *
     * @return String
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * Gets the complete SQL string formed by the current specifications of this QueryBuilder.
     *
     * @return string The SQL query string.
     */
    public function getSQL(): string
    {
        return $this->buildQuery()->getQb()->getSQL();
    }

    /**
     * Gets all defined query parameters for the query being constructed indexed by parameter index or name.
     *
     * @return mixed[] The currently defined query parameters indexed by parameter index or name.
     */
    public function getParameters()
    {
        return $this->buildQuery()->getQb()->getParameters();
    }

    /**
     * Return SQL and Parameters values
     *
     * @return array
     */
    public function debugSql(): array
    {
        return [
            'SQL' => $this->getSQL(),
            'Parameters' => $this->getParameters(),
        ];
    }

    /**
     * Retourne la pagination
     *
     * @return Pagination
     */
    public function getPagination(): Pagination
    {
        return $this->pagination;
    }
}
