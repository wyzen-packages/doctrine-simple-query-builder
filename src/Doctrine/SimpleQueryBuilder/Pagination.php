<?php

/**
 * Pagination
 * @author WYZEN
 *
 */

declare(strict_types=1);

namespace Wyzen\Doctrine\SimpleQueryBuilder;

use JsonSerializable;

final class Pagination implements JsonSerializable
{
    /**
     * Rows per page
     * @var int
     */
    private $limit;

    /**
     * current page
     * @var int
     */
    private $page;

    /**
     * total pages
     * @var int
     */
    private $pages;

    /**
     * total global rows
     * @var int
     */
    private $results;

    /**
     * Name constructor.
     *
     * @param integer|null $limit
     * @param integer|null $page
     * @param integer|null $pages
     * @param integer|null $results
     */
    public function __construct(?int $limit = 0, ?int $page = 1, ?int $pages = 1, ?int $results = 0)
    {
        $this->limit   = \intval($limit);
        $this->page    = \intval($page);
        $this->pages   = \intval($pages);
        $this->results = \intval($results);
    }

    /**
     * getter
     *
     * @return integer
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * getter
     *
     * @return integer
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * getter
     *
     * @return integer
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * getter
     *
     * @return integer
     */
    public function getResults(): int
    {
        return $this->results;
    }

    /**
     * setter
     * @param int $value
     * @return self
     */
    public function setLimit(int $value): self
    {
        $this->limit = $value;
        return $this;
    }

    /**
     * setter
     * @param int $value
     * @return self
     */
    public function setPage(int $value): self
    {
        $this->page = $value;
        return $this;
    }

    /**
     * setter
     * @param int $value
     * @return self
     */
    public function setPages(int $value): self
    {
        $this->pages = $value;
        return $this;
    }

    /**
     * setter
     * @param int $total_rows
     * @return self
     */
    public function setResults(int $total_rows): self
    {
        $this->results = $total_rows;

        // Set the total pages
        $pages = $this->getLimit() ? \intval(ceil($total_rows / $this->getLimit())) : 0;
        $this->setPages($pages);

        return $this;
    }

    /**
     * Clear pagination
     *
     * @return self
     */
    public function clear(): self
    {
        $this
          ->setLimit(0)
          ->setPage(0)
          ->setPages(0)
          ->setResults(0);
        return $this;
    }

    /**
     * return is active pagination
     *
     * @return boolean
     */
    public function isActive(): bool
    {
        return $this->getLimit() > 0 && $this->getPage() > 0;
    }

    public function jsonSerialize(): array
    {
        return [
            'limit' => \intval($this->limit),
            'page' => \intval($this->page),
            'pages' => \intval($this->pages),
            'results' => \intval($this->results),
        ];
    }
}
