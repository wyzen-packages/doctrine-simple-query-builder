<?php

/**
 * Simple Query Builder
 * @author WYZEN
 *
 */

declare(strict_types=1);

namespace Wyzen\Doctrine\SimpleQueryBuilder\Tests;

use Doctrine\DBAL\Connection;
use Wyzen\Doctrine\SimpleQueryBuilder\SimpleQueryBuilderAbstract;

class SimpleQueryBuilderCustom extends SimpleQueryBuilderAbstract
{
    public const LIMIT = 4;

    /** @inheritDoc */
    public function __construct(Connection $conn, ?string $className = null)
    {
        parent::__construct($conn, $className);
    }
}
