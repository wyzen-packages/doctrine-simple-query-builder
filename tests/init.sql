delete from users_profil;
delete from profil;
delete from users;

INSERT INTO users (user_id, name, email, create_at) VALUES(1, 'Abel Warren', 'havbxl@-go-uj.com', 1222259787136);
INSERT INTO users (user_id, name, email, create_at) VALUES(2, 'Erick Valentine', 'iueep@-q-bs-.com', 1216926715264);
INSERT INTO users (user_id, name, email, create_at) VALUES(3, 'Janice Payne', 'dvtu0@------.org', 1201386937216);
INSERT INTO users (user_id, name, email, create_at) VALUES(4, 'Gretchen Mason', 'nkls.mughxf@q--g--.org', 1224605955456);
INSERT INTO users (user_id, name, email, create_at) VALUES(5, 'Lawanda Noble', 'mpzqp@yb-x-t.com', 1201264494080);
INSERT INTO users (user_id, name, email, create_at) VALUES(6, 'Robbie Baird', 'yyax5@d-tk--.com', 1217541991808);
INSERT INTO users (user_id, name, email, create_at) VALUES(7, 'Carla Compton', 'ctuf.qacaf@k-n--z.net', 1223432965504);
INSERT INTO users (user_id, name, email, create_at) VALUES(8, 'Heath Stafford', 'bovq@xmm--s.com', 1217329495424);
INSERT INTO users (user_id, name, email, create_at) VALUES(9, 'Kendra Stevenson', 'neit@-sy-qj.com', 1206330866048);
INSERT INTO users (user_id, name, email, create_at) VALUES(10, 'Brandie Chase', 'potq3@sgwcc-.net', 1226094708096);
INSERT INTO users (user_id, name, email, create_at) VALUES(13, 'Rose Kirk', 'dnnb64@--owk-.net', 1224650952064);
INSERT INTO users (user_id, name, email, create_at) VALUES(14, 'Ernest Lam', 'decx@qowq-o.org', 1201244682496);
INSERT INTO users (user_id, name, email, create_at) VALUES(16, 'Randal Jackson', 'yhww@i---je.org', 1212156725632);
INSERT INTO users (user_id, name, email, create_at) VALUES(17, 'Ismael Holt', 'nitlx@vt-d-d.com', 1224793144704);
INSERT INTO users (user_id, name, email, create_at) VALUES(18, 'Keri Williams', 'fvho@wutq.z--pv-.com', 1203417765760);
INSERT INTO users (user_id, name, email, create_at) VALUES(20, 'Cameron Miranda', 'vhjjb.criwkwf@d-v-fr.net', 1226491145600);
INSERT INTO users (user_id, name, email, create_at) VALUES(21, 'Roberta Harrison', 'fqpgux24@i--cby.com', 1203971801984);
INSERT INTO users (user_id, name, email, create_at) VALUES(22, 'Colby Wilkerson', 'imuh@-r-rkb.com', 1221654273408);
INSERT INTO users (user_id, name, email, create_at) VALUES(23, 'Cornelius Johnson', 'fpdh1@----wz.net', 1227927119232);
INSERT INTO users (user_id, name, email, create_at) VALUES(25, 'Teddy Hoover', 'huheeh@w----n.com', 1230737401216);
INSERT INTO users (user_id, name, email, create_at) VALUES(26, 'Lillian Duran', 'zenh@------.com', 1200449488512);
INSERT INTO users (user_id, name, email, create_at) VALUES(27, 'Dianna Oliver', 'yjybs13@--m-cl.net', 1216217862528);
INSERT INTO users (user_id, name, email, create_at) VALUES(28, 'Geoffrey Aguirre', 'ienq.tlihtq@---ekl.com', 1203515704192);
INSERT INTO users (user_id, name, email, create_at) VALUES(29, 'Melissa Atkinson', 'crbn@q---wa.org', 1230053289344);
INSERT INTO users (user_id, name, email, create_at) VALUES(30, 'Moses Downs', 'aejh@-----l.com', 1214295137664);
INSERT INTO users (user_id, name, email, create_at) VALUES(31, 'Franklin Jones', 'wrcg7@-t--fo.net', 1204152183168);
INSERT INTO users (user_id, name, email, create_at) VALUES(32, 'Arlene Andersen', 'lkouu834@-y--oy.net', 1216298590592);
INSERT INTO users (user_id, name, email, create_at) VALUES(33, 'Kathleen Novak', 'esmuf7@tvt--z.org', 1216505501056);
INSERT INTO users (user_id, name, email, create_at) VALUES(34, 'Sheldon Stuart', 'oxhy@-ey--m.net', 1213797967232);
INSERT INTO users (user_id, name, email, create_at) VALUES(35, 'Ivan Humphrey', 'hhsr9@-c-l-t.com', 1202100144512);

INSERT INTO profil (profil_id, description, roles, is_actif) VALUES('GUEST', 'Tony has free time. Anne is walking. Tony bought new car. Tony bought new car. Tony is shopping. ', NULL, 0);
INSERT INTO profil (profil_id, description, roles, is_actif) VALUES('ADMIN', 'Tony is shopping. John has free time. Tony is walking. ', NULL, 1);
INSERT INTO profil (profil_id, description, roles, is_actif) VALUES('READER', 'Tony bought new car. Anne bought new car. Anne is walking. John has free time. John has free time. ', NULL, 1);
INSERT INTO profil (profil_id, description, roles, is_actif) VALUES('WRITER', 'none', NULL, 1);

INSERT INTO users_profil (users_profil_id, fk_users, fk_profil, last_connection) VALUES(1, 34, 'READER', 1216926715264);
INSERT INTO users_profil (users_profil_id, fk_users, fk_profil, last_connection) VALUES(2, 29, 'WRITER', NULL);
INSERT INTO users_profil (users_profil_id, fk_users, fk_profil, last_connection) VALUES(3, 25, 'READER', 1201264494080);
INSERT INTO users_profil (users_profil_id, fk_users, fk_profil, last_connection) VALUES(4, 25, 'ADMIN', 1223432965504);
INSERT INTO users_profil (users_profil_id, fk_users, fk_profil, last_connection) VALUES(5, 3, 'GUEST', 1206330866048);

drop view if exists v_dto_users_profil;
create view v_dto_users_profil as
select
user_id ,
name,
email,
create_at ,
profil_id,
description profil_description,
is_actif profil_is_actif
from users u inner join users_profil up ON (up.fk_users=u.user_id) inner join profil p ON (up.fk_profil=p.profil_id);

drop view if exists v_users_profil;
create view v_users_profil as
select * from v_dto_users_profil;
