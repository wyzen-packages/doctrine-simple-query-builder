<?php

namespace Wyzen\Doctrine\SimpleQueryBuilder\Tests;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Connection;
use Wyzen\Doctrine\SimpleQueryBuilder\SimpleQueryBuilderOperator;
use PHPUnit\Framework\TestCase;
use Wyzen\Doctrine\SimpleQueryBuilder\Pagination;
use Wyzen\Doctrine\SimpleQueryBuilder\SimpleQueryBuilder;
use Wyzen\Doctrine\SimpleQueryBuilder\SimpleQueryBuilderAbstract;

class SimpleQueryBuilderTest extends TestCase
{
    private static $db_file         = __DIR__ . '/tests.db';
    public const USERS_COUNT        = 30;
    public const PROFIL_COUNT       = 4;
    public const USERS_PROFIL_COUNT = 5;

    /** @var Connection */
    private static $conn = null;

    private $profil = [
        'admin' => [
            'profil_id' => 'ADMIN',
            'description' => 'desc ADMIN',
            'roles' => null,
            'is_actif' => '1',
        ],
        'guest' => [
            'profil_id' => 'GUEST',
            'description' => 'desc GUEST',
            'roles' => null,
            'is_actif' => '0',
        ],
        'reader' => [
            'profil_id' => 'READER',
            'description' => 'desc READER',
            'roles' => null,
            'is_actif' => '1',
        ],
        'writer' => [
            'profil_id' => 'WRITER',
            'description' => 'desc WRITER',
            'roles' => null,
            'is_actif' => '1',
        ],
    ];

    public static function setUpBeforeClass(): void
    {
        $config = new \Doctrine\DBAL\Configuration();
        $config->setAutoCommit(false);

        $connectionParams = [
            'driver' => 'pdo_sqlite',
            'url' => 'sqlite:///' . self::$db_file,
        ];

        if (!\file_exists(self::$db_file)) {
            die("file not exists: " . self::$db_file);
        }
        try {
            self::$conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        } catch (DBALException $ex) {
            die($ex->getMessage());
        }
    }

    public static function tearDownAfterClass(): void
    {
    }

    public function testConstruct()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);
        $this->assertInstanceOf(SimpleQueryBuilderAbstract::class, $sqb);

        $sqb_custom = new SimpleQueryBuilderCustom(self::$conn);
        $this->assertInstanceOf(SimpleQueryBuilderAbstract::class, $sqb_custom);
    }

    /**
     * Undocumented function
     * @testdox Construction des noms de table/vue
     * @return void
     */
    public function testSetGetTable()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);

        $sqb->setTable('users');
        $tablename = $sqb->getTable();
        $this->assertEquals('users', $tablename);

        $sqb->setTable('UsersDtoRepository');
        $tablename = $sqb->getTable();
        $this->assertEquals('v_dto_users', $tablename);

        $sqb->setTable('UsersRepository');
        $tablename = $sqb->getTable();
        $this->assertEquals('users', $tablename);

        $classname = __CLASS__ . '\UsersProfilDtoRepository';
        $sqb->setTable($classname);
        $tablename = $sqb->getTable();
        $this->assertEquals('v_dto_users_profil', $tablename);

        $classname = __CLASS__ . '\UsersProfilRepository';
        $sqb->setTable($classname);
        $tablename = $sqb->getTable();
        $this->assertEquals('users_profil', $tablename);

        $sqb->setTable('UsersProfil');
        $tablename = $sqb->getTable();
        $this->assertEquals('UsersProfil', $tablename);

        $sqb->setTable('v_sample_view');
        $tablename = $sqb->getTable();
        $this->assertEquals('v_sample_view', $tablename);

        $sqb->setTable('UsersProfilDto');
        $this->assertEquals('UsersProfilDto', $sqb->getTable());
    }

    public function testClearAll()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::EQ);
        $sqb->addGroup('name')->addOrder('name', 'desc');

        $expectedQuery = 'SELECT * FROM users WHERE name = :name_0 GROUP BY name ORDER BY name desc';
        $this->assertEquals($expectedQuery, $sqb->getSQL());

        $expectedQuery = 'SELECT * FROM users';
        $sqb->clearAll();
        $this->assertEquals($expectedQuery, $sqb->getSQL());
    }

    /**
     * @testdox Operator EQ/NE
     *
     * @return void
     */
    public function testOperatorEqNe()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::EQ);

        $expectedQuery = 'SELECT * FROM users WHERE name = :name_0';
        $expectedValue = [
            "name_0" => "admin"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());


        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::NE);

        $expectedQuery = 'SELECT * FROM users WHERE name <> :name_0';
        $expectedValue = [
            "name_0" => "admin"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    /**
     * @testdox Operator LT/LE
     *
     * @return void
     */
    public function testOperatorLtLe()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::LT);

        $expectedQuery = 'SELECT * FROM users WHERE name < :name_0';
        $expectedValue = [
            "name_0" => "admin"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::LE);

        $expectedQuery = 'SELECT * FROM users WHERE name <= :name_0';
        $expectedValue = [
            "name_0" => "admin"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    /**
     * @testdox Operator GT/GE
     *
     * @return void
     */
    public function testOperatorGtGe()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::GT);

        $expectedQuery = 'SELECT * FROM users WHERE name > :name_0';
        $expectedValue = [
            "name_0" => "admin"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::GE);

        $expectedQuery = 'SELECT * FROM users WHERE name >= :name_0';
        $expectedValue = [
            "name_0" => "admin"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    /**
     * @testdox Operator IN/NOT IN
     *
     * @return void
     */
    public function testOperatorInNotIn()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', ['admin', 'guest'], SimpleQueryBuilderOperator::IN);

        $expectedQuery = 'SELECT * FROM users WHERE name IN(:name_0)';
        $expectedValue = [
            "name_0" => ['admin', 'guest']
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', ['admin', 'guest'], SimpleQueryBuilderOperator::NOTIN);

        $expectedQuery = 'SELECT * FROM users WHERE name NOT IN(:name_0)';
        $expectedValue = [
            "name_0" => ['admin', 'guest']
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    /**
     * @testdox Operator BETWEEN/NOT BETWEEN
     *
     * @return void
     */
    public function testOperatorBetweenNotBetween()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('user_id', [100, 200], SimpleQueryBuilderOperator::BETWEEN);

        $expectedQuery = 'SELECT * FROM users WHERE user_id BETWEEN :user_id_01 AND :user_id_02';
        $expectedValue = [
            'user_id_01' => 100,
            'user_id_02' => 200
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('user_id', [100, 200], SimpleQueryBuilderOperator::NOTBETWEEN);

        $expectedQuery = 'SELECT * FROM users WHERE (user_id < :user_id_01) AND (user_id > :user_id_02)';
        $expectedValue = [
            'user_id_01' => 100,
            'user_id_02' => 200
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    /**
     * @testdox Operator IS NULL/ IS NOT NULL
     *
     * @return void
     */
    public function testOperatorIsnullIsnotnull()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('user_id', SimpleQueryBuilderOperator::ISNULL);

        $expectedQuery = 'SELECT * FROM users WHERE user_id IS NULL';
        $this->assertEquals($expectedQuery, $sqb->getSQL());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('user_id', null, SimpleQueryBuilderOperator::EQ);

        $expectedQuery = 'SELECT * FROM users WHERE user_id IS NULL';

        $this->assertEquals($expectedQuery, $sqb->getSQL());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('user_id', SimpleQueryBuilderOperator::ISNOTNULL);

        $expectedQuery = 'SELECT * FROM users WHERE user_id IS NOT NULL';

        $this->assertEquals($expectedQuery, $sqb->getSQL());
    }

    public function testBooleanValueFalse()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('actif', false);

        $expectedQuery = 'SELECT * FROM users WHERE actif = :actif_0';
        $this->assertEquals($expectedQuery, $sqb->getSQL());

        $expectedValue = [
            "actif_0" => 0
        ];
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    public function testBooleanValueTrue()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('actif', true);

        $expectedQuery = 'SELECT * FROM users WHERE actif = :actif_0';
        $this->assertEquals($expectedQuery, $sqb->getSQL());

        $expectedValue = [
            "actif_0" => 1
        ];
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    /**
     * @testdox Operator LIKE/NOT LIKE
     *
     * @return void
     */
    public function testOperatorLikeNotlike()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', '%admin%', SimpleQueryBuilderOperator::LIKE);

        $expectedQuery = 'SELECT * FROM users WHERE name LIKE :name_0';
        $expectedValue = [
            "name_0" => "%admin%"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());

        $sqb->clearAll();
        $sqb->setTable('users');
        $sqb->addFilter('name', '%admin%', SimpleQueryBuilderOperator::NOTLIKE);

        $expectedQuery = 'SELECT * FROM users WHERE name NOT LIKE :name_0';
        $expectedValue = [
            "name_0" => "%admin%"
        ];

        $this->assertEquals($expectedQuery, $sqb->getSQL());
        $this->assertEquals($expectedValue, $sqb->getParameters());
    }

    public function testQueryCountProfil()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll()->setTable('profil');

        $nb = $sqb->executeRowCount();
        $this->assertEquals(self::PROFIL_COUNT, $nb);
    }

    public function testAddCustomFilterFilterValue()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);

        $expected = [
            [
                "user_id" => 33,
                "name" => "Kathleen Novak",
                "email" => "esmuf7@tvt--z.org",
                "create_at" => 1216505501056
            ],
        ];

        $sqb->clearAll()
            ->setTable('users')
            ->addCustomFilter('user_id', 33);
        $data = $sqb->fetchAll();
        $this->assertEquals($expected, $data);
    }

    public function testAddCustomFilterFilterIsNotNull()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);
        $sqb->clearAll()
            ->setTable('users')
            ->addCustomFilter('users.name', SimpleQueryBuilderOperator::ISNOTNULL);
        $data = $sqb->fetchAll();
        $this->assertEquals(self::USERS_COUNT, count($data));
    }

    public function testAddCustomFilterFilterValueOperatorGt()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);

        $expected = [
            [
                "user_id" => 34,
                "name" => "Sheldon Stuart",
                "email" => "oxhy@-ey--m.net",
                "create_at" => 1213797967232,
            ],
            [
                "user_id" => 35,
                "name" => "Ivan Humphrey",
                "email" => "hhsr9@-c-l-t.com",
                "create_at" => 1202100144512,
            ]
        ];
        $sqb->clearAll()
            ->setTable('users')
            ->addCustomFilter('users.user_id', 33, SimpleQueryBuilderOperator::GT);
        $data = $sqb->fetchAll();
        $this->assertEquals($expected, $data);
    }

    public function testAddCustomFilterFilterOnly()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);
        $expected = [
            [
                "user_id" => 33,
                "name" => "Kathleen Novak",
                "email" => "esmuf7@tvt--z.org",
                "create_at" => 1216505501056,
            ]
        ];
        $sqb->clearAll()
            ->setTable('users')
            ->addCustomFilter("name='Kathleen Novak'");
        $data = $sqb->fetchAll();
        $this->assertEquals($expected, $data);
    }

    public function testAddCustomFilterFilterOnlyWithOr()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);

        $expected = [
            [
                "user_id" => 33,
                "name" => "Kathleen Novak",
                "email" => "esmuf7@tvt--z.org",
                "create_at" => 1216505501056,
            ],
            [
                "user_id" => 34,
                "name" => "Sheldon Stuart",
                "email" => "oxhy@-ey--m.net",
                "create_at" => 1213797967232,
            ]
        ];

        $sqb->clearAll()
            ->setTable('users')
            ->addCustomFilter('user_id=33 or user_id=34');
        $data = $sqb->fetchAll();
        $this->assertEquals($expected, $data);
    }

    public function testSlugify()
    {
        $filter = 'users.user_id=30 or users.user_id=34';
        $expected = 'users_user_id_30_or_users_user_id_34';
        $slug = SimpleQueryBuilder::slugify($filter);
        $this->assertEquals($expected, $slug);


        $filter = '.!_Hé toi  __ ! Viens là .';
        $expected = 'he_toi_viens_la';
        $slug = SimpleQueryBuilder::slugify($filter);
        $this->assertEquals($expected, $slug);
    }

    public function testGetRowsProfil()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb->clearAll()->setTable('profil')->addOrder('profil_id');
        $rows = $sqb->fetchAll();

        $expected = \array_values($this->profil);
        $this->assertEquals($expected, $rows);

        /**
         * order DESC
         */
        $sqb->clearAll()->setTable('profil')->addOrder('profil_id', 'desc');
        $rows = $sqb->fetchAll();
        $this->assertEquals(\array_reverse($expected), $rows);
    }

    public function testGetRowsProfilFetchAll()
    {
        $sqb        = new SimpleQueryBuilderCustom(self::$conn);
        $pagination = new Pagination(SimpleQueryBuilderCustom::LIMIT);
        $sqb->clearAll()->setTable('profil')->addOrder('profil_id');
        $sqb->setPagination($pagination);
        $rows = $sqb->fetchAll('profil_id');

        $expected = \array_values($this->profil);
        $this->assertEquals($expected, $rows);
    }

    public function testGetRowProfilWriter()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('profil')
            ->addFilter('profil_id', 'ADMIN');
        $rows = $sqb->fetchAll();
        $this->assertIsArray($rows);
        $this->assertEquals(1, count($rows));

        $expected = $this->profil['admin'];
        $this->assertEquals($expected, $rows[0]);
    }

    public function testGetRowsProfilAdminAndWriter()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('profil')
            ->addFilter('profil_id', ['ADMIN', 'WRITER'], SimpleQueryBuilderOperator::IN);
        $rows = $sqb->fetchAll();
        $this->assertIsArray($rows);
        $this->assertEquals(2, count($rows));

        $expected = [
            $this->profil['admin'],
            $this->profil['writer']
        ];
        $this->assertEquals($expected, $rows);
    }

    /**
     * @testdox Conversion d'un EQ en IN si value est un tableau
     *
     * @return void
     */
    public function testConvertEQtoIN()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('profil')
            ->addFilter('profil_id', ['ADMIN', 'WRITER'], SimpleQueryBuilderOperator::EQ);
        $rows = $sqb->fetchAll();

        $this->assertIsArray($rows);
        $this->assertEquals(2, count($rows));

        $expected = [
            $this->profil['admin'],
            $this->profil['writer']
        ];
        $this->assertEquals($expected, $rows);

        // Mode implicite
        $sqb
            ->clearAll()
            ->setTable('profil')
            ->addFilter('profil_id', ['ADMIN', 'WRITER']);
        $rows = $sqb->fetchAll();

        $this->assertIsArray($rows);
        $this->assertEquals(2, count($rows));

        $expected = [
            $this->profil['admin'],
            $this->profil['writer']
        ];
        $this->assertEquals($expected, $rows);
    }

    /**
     * @testdox Conversion d'un NE en NOTIN si value est un tableau
     *
     * @return void
     */
    public function testConvertNEtoNOTIN()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('profil')
            ->addFilter('profil_id', ['ADMIN', 'WRITER'], SimpleQueryBuilderOperator::NE);
        $rows = $sqb->fetchAll();

        $this->assertIsArray($rows);
        $this->assertEquals(2, count($rows));

        $expected = [
            $this->profil['guest'],
            $this->profil['reader']
        ];
        $this->assertEquals($expected, $rows);
    }

    public function testGetRowsProfilNotAdminAndNotWriter()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('profil')
            ->addFilter('profil_id', ['ADMIN', 'WRITER'], SimpleQueryBuilderOperator::NOTIN);
        $rows = $sqb->fetchAll();

        $this->assertIsArray($rows);
        $this->assertEquals(2, count($rows));

        $expected = [
            $this->profil['guest'],
            $this->profil['reader']
        ];
        $this->assertEquals($expected, $rows);
    }

    public function testGetRowsUsersBetween3And10()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('users')
            ->addFilter('user_id', [3, 10], SimpleQueryBuilderOperator::BETWEEN);
        $rows = $sqb->fetchAll();

        $this->assertIsArray($rows);
        $this->assertEquals(8, count($rows));
    }

    public function testGetRowsViewWithUsersBetween3And10()
    {
        $sqb = new SimpleQueryBuilderCustom(self::$conn);
        $sqb
            ->clearAll()
            ->setTable('UsersProfilDtoRepository')
            ->addFilter('profil_id', ['ADMIN', 'READER'], SimpleQueryBuilderOperator::IN);
        $rows = $sqb->fetchAll();

        $this->assertIsArray($rows);
        $this->assertEquals(3, count($rows));
    }

    public function testPagination()
    {
        /** @var SimpleQueryBuilder */
        $sqb = new SimpleQueryBuilderCustom(self::$conn);

        // Lecture sans limit
        $sqb
            ->clearAll()
            ->setTable('users')
            ->addFilter('email', '%.com%', SimpleQueryBuilderOperator::LIKE)
            ->addOrder('name');
        $rows    = $sqb->fetchAll();
        $nbTotal = count($rows);
        $this->assertIsArray($rows);
        $this->assertEquals(15, $nbTotal);

        $pagination = new Pagination(SimpleQueryBuilderCustom::LIMIT);

        // Limit $limit page 1
        $sqb->setPagination($pagination);
        $rows = $sqb->fetchAll();
        $this->assertEquals(SimpleQueryBuilderCustom::LIMIT, count($rows));

        $pagination = $sqb->getPagination();
        $this->assertEquals(new Pagination(SimpleQueryBuilderCustom::LIMIT, 1, 4, $nbTotal), $pagination);

        // Limit $limit page 2
        $pagination->setPage(2);
        $sqb->setPagination($pagination);
        $rows = $sqb->fetchAll();
        $this->assertEquals(SimpleQueryBuilderCustom::LIMIT, count($rows));
        $pagination = $sqb->getPagination();
        $this->assertEquals(new Pagination(SimpleQueryBuilderCustom::LIMIT, 2, 4, $nbTotal), $pagination);

        // Limit 4 page 2
        $sqb->setPagination(new Pagination(SimpleQueryBuilderCustom::LIMIT, 2));
        $rows = $sqb->fetchAll();
        $this->assertEquals(4, count($rows));
        $pagination = $sqb->getPagination();
        $this->assertEquals(new Pagination(SimpleQueryBuilderCustom::LIMIT, 2, 4, $nbTotal), $pagination);
    }

    public function testLoopPagination()
    {
        $countRow = 0;
        $limit    = 4;

        /** @var SimpleQueryBuilder */
        $sqb = new SimpleQueryBuilderCustom(self::$conn);

        // Lecture sans limit
        $sqb
            ->setTable('users')
            ->addOrder('name');
        $rows = $sqb->fetchAll();

        // Get total rows without pagination
        $totalRows = count($rows);

        // Set pagination
        $pagination = new Pagination($limit);

        // Limit $limit page 1
        $sqb->setPagination($pagination);
        $rows      = $sqb->fetchAll();
        $countRow +=  count($rows);
        $this->assertEquals($limit, count($rows));

        $pagination = $sqb->getPagination();
        $totalPage  = $pagination->getPages();

        // Parcours des pages
        for ($page = 2; $page <= $pagination->getPages(); $page++) {
            $pagination->setPage($page);
            $rows      = $sqb->fetchAll();
            $countRow +=  count($rows);
            $this->assertLessThanOrEqual($limit, count($rows));
        }
        // Test si toutes les pages sont parcourues
        $this->assertEquals($totalPage + 1, $page);

        // Test si le nombre de lignes lues par page correspond au total
        $this->assertEquals($totalRows, $countRow);
    }

    public function testSetLimit1()
    {
        /** @var SimpleQueryBuilder */
        $sqb = new SimpleQueryBuilderCustom(self::$conn);

        // Lecture sans limit
        $sqb
            ->setTable('users')
            ->addOrder('name')
            ->setLimit(1);
        $rows = $sqb->fetchAll();
        $this->assertEquals(1, count($rows));

        $sqb
            ->clearAll()
            ->setTable('users')
            ->addOrder('name');
        $rows = $sqb->fetchAll();
        $this->assertEquals(self::USERS_COUNT, count($rows));
    }

    public function testRowCount()
    {
        /** @var SimpleQueryBuilder */
        $sqb = new SimpleQueryBuilderCustom(self::$conn);

        // Lecture sans limit
        $sqb
            ->setTable('users')
            ->addOrder('name');
        $nbRows = $sqb->executeRowCount();
        $this->assertEquals(self::USERS_COUNT, $nbRows);

        // Count all rows
        $sqb->clearAll()
            ->setTable('users_profil');
        $nbRows = $sqb->executeRowCount();
        $this->assertEquals(5, $nbRows);

        //Count distinct by column name
        $nbRows = $sqb->executeRowCount('fk_profil');
        $this->assertEquals(4, $nbRows);
    }

    public function testGroupByMultipleField()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);
        $sqb->clearAll()
            ->setDistinct()
            ->setTable('users')
            ->addFields(['name', 'age', 'address'])
            ->addFilter('name', 'admin', SimpleQueryBuilderOperator::EQ)
            ->addGroup('name')
            ->addGroup('age');
        $expectedQuery = "SELECT DISTINCT name, age, address FROM users WHERE name = :name_0 GROUP BY name, age";
        $this->assertEquals($expectedQuery, $sqb->getSQL());
    }

    public function testGroupByMultipleFieldArray()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);
        $sqb->clearAll()
            ->setDistinct()
            ->setTable('users')
            ->addFields(['name', 'age', 'address'])
            ->addFilter('name', 'admin', SimpleQueryBuilderOperator::EQ)
            ->addGroups(['name', 'age']);
        $expectedQuery = "SELECT DISTINCT name, age, address FROM users WHERE name = :name_0 GROUP BY name, age";
        $this->assertEquals($expectedQuery, $sqb->getSQL());
    }

    public function testIsDistinct()
    {
        $sqb = new SimpleQueryBuilder(self::$conn);
        $sqb->clearAll();
        $sqb->setDistinct();
        $sqb->setTable('users');
        $sqb->addFields(['name', 'age', 'address']);
        $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::EQ);

        $expectedQuery = "SELECT DISTINCT name, age, address FROM users WHERE name = :name_0";
        $this->assertEquals($expectedQuery, $sqb->getSQL());
    }

    public function testTransaction()
    {
        try {
            $sqb = new SimpleQueryBuilder(self::$conn);

            $sqb->getConn()->beginTransaction();

            $sqb->clearAll();
            $sqb->setDistinct();
            $sqb->setTable('users');
            $sqb->addFields(['name', 'age', 'address']);
            $sqb->addFilter('name', 'admin', SimpleQueryBuilderOperator::EQ);

            $expectedQuery = "SELECT DISTINCT name, age, address FROM users WHERE name = :name_0";
            $this->assertEquals($expectedQuery, $sqb->getSQL());
        } catch (\Exception $ex) {
            $sqb->getConn()->rollBack();
        }
    }
}
